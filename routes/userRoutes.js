const {Router} = require('express');
const UserService = require('../services/userService');
const {createUserValid, updateUserValid} = require('../middlewares/user.validation.middleware');
const {responseMiddleware} = require('../middlewares/response.middleware');
const {
    USER_EXITS,
    USER_NOT_FOUND,
    USERS_NOT_FOUND,
    USER_NOT_CREATED,
    USER_NOT_DELETED
} = require('../constants/responseMessagesError');

const router = Router();

router.post('/', createUserValid, (req, res, next) => {
    try {
        // TODO: Implement action
        const {body: userBody} = req;
        const {email, phoneNumber} = userBody;
        const emailExist = UserService.search({email});
        const phoneNumberExist = UserService.search({phoneNumber});
        if (!emailExist && !phoneNumberExist) {
            const user = UserService.create(userBody);
            user
                ? req.data = user
                : req.err = {
                    status: 400,
                    message: USER_NOT_CREATED
                };
        } else {
            req.err = {
                status: 400,
                message: USER_EXITS
            };
        }
    } catch (e) {
        req.err = {
            status: 400,
            message: e.message
        };
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/', (req, res, next) => {
    try {
        const users = UserService.getAll();
        if (users) {
            req.data = users;
        } else {
            req.err = {
                status: 404,
                message: USERS_NOT_FOUND
            };
        }
    } catch (e) {
        req.err = {
            status: 400,
            message: e.message
        };
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        const {id} = req.params;
        const user = UserService.getOne({id});
        if (user) {
            req.data = user;
        } else {
            req.err = {
                status: 404,
                message: USER_NOT_FOUND
            };
        }
    } catch (e) {
        req.err = {
            status: 400,
            message: e.message
        };
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
    try {
        const {id} = req.params;
        const {body: dataToUpdate} = req;
        const userData = UserService.search({id});
        if (userData) {
            const {email, phoneNumber} = dataToUpdate;
            const emailExist = UserService.search({email});
            const phoneNumberExist = UserService.search({phoneNumber});
            (!emailExist && !phoneNumberExist)
                ? req.data = UserService.update(id, dataToUpdate)
                : req.err = {
                    status: 400,
                    message: USER_EXITS
                }
        } else {
            req.err = {
                status: 404,
                message: USER_NOT_FOUND
            };
        }
    } catch (e) {
        req.err = {
            status: 400,
            message: e.message
        };
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        const {id} = req.params;
        const user = UserService.getOne({id});
        if (user) {
            const userDelete = UserService.delete(id);
            userDelete
                ? req.data = userDelete
                : req.err = {
                    status: 400,
                    message: USER_NOT_DELETED
                }
        } else {
            req.err = {
                status: 404,
                message: USER_NOT_FOUND
            };
        }
    } catch (e) {
        req.err = {
            status: 400,
            message: e.message
        };
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;
