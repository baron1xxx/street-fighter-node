const {Router} = require('express');
const FightService = require('../services/fightService');
const { createFightValid, updateFightValid } = require('../middlewares/fight.validation.middleware');
const {responseMiddleware} = require('../middlewares/response.middleware');
const ErrorHandler = require('../error/ErrorHandler');
const {
    FIGHT_NOT_FOUND,
    FIGHTS_NOT_FOUND,
    FIGHT_NOT_CREATED,
    FIGHT_NOT_UPDATED,
    FIGHT_NOT_DELETED
} = require('../constants/responseMessagesError');


const router = Router();

router.post('/', createFightValid, (req, res, next) => {
    try {
        const {body: fightBody} = req;
        const fight = FightService.create(fightBody);
        if (!fight) {
            throw new ErrorHandler(FIGHT_NOT_CREATED, 400)
        }
        req.data = fight;

    } catch (e) {
        req.err = {
            status: e.status,
            message: e.message
        }
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/', (req, res, next) => {
    try {
        const fights = FightService.getAll();
        if (!fights) {
            throw new ErrorHandler(FIGHTS_NOT_FOUND, 404)
        }
        req.data = fights;

    } catch (e) {
        req.err = {
            status: e.status,
            message: e.message
        };
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        const {id} = req.params;
        const fight = FightService.getOne({id});
        if (!fight) {
            throw new ErrorHandler(FIGHT_NOT_FOUND, 404)
        }
        req.data = fight;
    } catch (e) {
        req.err = {
            status: e.status,
            message: e.message
        };
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', updateFightValid, (req, res, next) => {
    try {
        const {id} = req.params;
        const {body: dataToUpdate} = req;
        const fightData = FightService.search({id});
        if (!fightData) {
            throw new ErrorHandler(FIGHT_NOT_FOUND, 404)
        }
        const updateFight = FightService.update(id, dataToUpdate);
        if (!updateFight) {
            throw new ErrorHandler(FIGHT_NOT_UPDATED, 400)
        }
        req.data = updateFight;

    } catch (e) {
        req.err = {
            status: e.status,
            message: e.message
        };
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        const {id} = req.params;
        const fightData = FightService.search({id});
        if (!fightData) {
            throw new ErrorHandler(FIGHT_NOT_FOUND, 404)
        }
        const deletedFight = FightService.delete(id);
        if (!deletedFight) {
            throw new ErrorHandler(FIGHT_NOT_DELETED, 400)
        }
        req.data = deletedFight;

    } catch (e) {
        req.err = {
            status: e.status,
            message: e.message
        };
    } finally {
        next();
    }
}, responseMiddleware);


module.exports = router;
