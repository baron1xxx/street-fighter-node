const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        const {body: userData} = req;
        const user = AuthService.login(userData);
        if (user) {
            req.data = user;
        } else {
            req.err = {
                status: 401,
                message: 'Unauthorized'
            }
        }
    } catch (e) {
        req.err = {
            status: 401,
            message: e.message
        }
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;
