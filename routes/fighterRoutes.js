const {Router} = require('express');
const FighterService = require('../services/fighterService');
const {responseMiddleware} = require('../middlewares/response.middleware');
const {createFighterValid, updateFighterValid} = require('../middlewares/fighter.validation.middleware');
const {
    FIGHTER_EXITS,
    FIGHTER_NOT_FOUND,
    FIGHTERS_NOT_FOUND,
    FIGHTER_NOT_CREATED,
    FIGHTER_NOT_DELETED
} = require('../constants/responseMessagesError');

const router = Router();

router.post('/', createFighterValid, (req, res, next) => {
    try {
        const {body: fighterBody} = req;
        const {name} = fighterBody;
        const fighterData = FighterService.search({name});
        if (!fighterData) {
            const fighter = FighterService.create(fighterBody);
            fighter
                ? req.data = fighter
                : req.err = {
                    status: 400,
                    message: FIGHTER_NOT_CREATED
                };
        } else {
            req.err = {
                status: 400,
                message: FIGHTER_EXITS
            };
        }

    } catch (e) {
        req.err = {
            status: 400,
            message: e.message
        };
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/', (req, res, next) => {
    try {
        const fighter = FighterService.getAll();
        if (fighter) {
            req.data = fighter;
        } else {
            req.err = {
                status: 404,
                message: FIGHTERS_NOT_FOUND
            };
        }
    } catch (e) {
        req.err = {
            status: 400,
            message: e.message
        };
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        const {id} = req.params;
        const fighter = FighterService.getOne({id});
        if (fighter) {
            req.data = fighter;
        } else {
            req.err = {
                status: 404,
                message: FIGHTER_NOT_FOUND
            };
        }
    } catch (e) {
        req.err = {
            status: 400,
            message: e.message
        };
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
    try {
        const {id} = req.params;
        const {body: dataToUpdate} = req;
        const fighterData = FighterService.search({id});
        if (fighterData) {
            const {name} = dataToUpdate;
            const nameExist = FighterService.search({name});
            !nameExist
                ? req.data = FighterService.update(id, dataToUpdate)
                : req.err = {
                    status: 400,
                    message: FIGHTER_EXITS
                }
        } else {
            req.err = {
                status: 404,
                message: FIGHTER_NOT_FOUND
            };
        }
    } catch (e) {
        req.err = {
            status: 400,
            message: e.message
        };
    } finally {
        next();
    }
}, responseMiddleware);


router.delete('/:id', (req, res, next) => {
    try {
        const {id} = req.params;
        const fighter = FighterService.getOne({id});
        if (fighter) {
            const fighterDelete = FighterService.delete(id);
            fighterDelete
                ? req.data = fighterDelete
                : req.err = {
                    status: 400,
                    message: FIGHTER_NOT_DELETED
                }
        } else {
            req.err = {
                status: 404,
                message: FIGHTER_NOT_FOUND
            };
        }
    } catch
        (e) {
        req.err = {
            status: 400,
            message: e.message
        };
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;
