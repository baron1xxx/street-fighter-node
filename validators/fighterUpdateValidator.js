const Joi = require('@hapi/joi');
const {regExpExpressions} = require('../constants');

module.exports = Joi.object().keys({
    name: Joi.string().trim().min(3).max(20).regex(new RegExp(regExpExpressions.name))
        .messages({
            'string.base': 'Name must be string',
            'string.min': 'Minimum {#limit} symbol ',
            'string.max': 'Maximum {#limit} symbol ',
            'string.pattern.base': 'Only letters.'
        }),
    health: Joi.number().integer().min(0).max(100)
        .messages({
            'string.base': 'health must be number',
            'string.integer': 'health must be integer number',
            'string.positive': 'health must be positive number',
            'string.min': 'Minimum {#limit}.',
            'string.max': 'Maximum {#limit}.'
        }),
    power: Joi.number().integer().positive().min(0).max(100)
        .messages({
            'string.base': 'power must be number',
            'string.integer': 'power must be integer number',
            'string.positive': 'power must be positive number',
            'string.min': 'Minimum {#limit}.',
            'string.max': 'Maximum {#limit}.'
        }),

    defense: Joi.number().integer().positive().min(0).max(100)
        .messages({
            'string.base': 'defense must be number',
            'string.integer': 'defense must be integer number',
            'string.positive': 'defense must be positive number',
            'string.min': 'Minimum {#limit}.',
            'string.max': 'Maximum {#limit}.'
        })
});
