const Joi = require('@hapi/joi');

module.exports = Joi.object().keys({
    fighter1: Joi.string().required().guid({version: ['uuidv4']})
        .messages({
            'string.base': 'name must be string',
            'string.required': 'name is required'
        }),
    fighter2: Joi.string().required().guid({version: ['uuidv4']})
        .messages({
            'string.base': 'name must be string',
            'string.required': 'name is required'
        }),
    log: Joi.array().items(
        Joi.object({
            fighter1Health: Joi.number().required().positive().min(0).max(100),
            fighter2Health: Joi.number().required().positive().min(0).max(100),
        })
    )
});
