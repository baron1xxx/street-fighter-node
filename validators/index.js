module.exports.userCreateValidator = require('./userCreateValidator');
module.exports.userUpdateValidator = require('./userUpdateValidator');
module.exports.fighterCreateValidator = require('./fighterCreateValidator');
module.exports.fighterUpdateValidator = require('./fighterUpdateValidator');
module.exports.fightCreateValidator = require('./fightCreateValidator');
module.exports.fightUpdateValidator = require('./fightUpdateValidator');

