const Joi = require('@hapi/joi');
const {regExpExpressions} = require('../constants');

module.exports = Joi.object({
    firstName: Joi.string().trim().min(3).max(20).regex(new RegExp(regExpExpressions.name))
        .messages({
            'string.base': 'firstName must be string',
            'string.min': 'Minimum {#limit} symbol ',
            'string.max': 'Maximum {#limit} symbol ',
            'string.pattern.base': 'Only letters.'
        }),
    lastName: Joi.string().trim().min(3).max(20).regex(new RegExp(regExpExpressions.name))
        .messages({
            'string.base': 'firstName must be string',
            'string.min': 'Minimum {#limit} symbol ',
            'string.max': 'Maximum {#limit} symbol ',
            'string.pattern.base': 'Only letters.'
        }),
    email: Joi.string().trim().regex(new RegExp(regExpExpressions.email))
        .messages({
            'string.base': 'Email must be string',
            'string.pattern.base': 'Email pattern ivan@ivan.com'
        }),
    phoneNumber: Joi.string().trim().regex(new RegExp(regExpExpressions.phoneNumber))
        .messages({
            'string.base': 'Phone number must be string',
            'string.pattern.base': 'Phone number pattern +380xxxxxxxxx'
        }),
    password: Joi.string().regex(new RegExp(regExpExpressions.password))
        .messages({
            'string.base': 'Password must be string',
            'string.required': 'Password is required',
            'string.pattern.base': 'Email pattern ivan@ivan.com'
        }),
});
