const Joi = require('@hapi/joi');
const {regExpExpressions} = require('../constants');

module.exports = Joi.object().keys({
    firstName: Joi.string().trim().required().min(3).max(20).regex(new RegExp(regExpExpressions.name))
        .messages({
            'string.base': 'firstName must be string',
            'string.min': 'Minimum {#limit} symbol ',
            'string.max': 'Maximum {#limit} symbol ',
            'string.pattern.base': 'Only letters.',
            'string.required': 'firstName is required'
        }),
    lastName: Joi.string().trim().required().min(3).max(20).regex(new RegExp(regExpExpressions.name))
        .messages({
            'string.base': 'lastName must be string',
            'string.min': 'Minimum {#limit} symbol ',
            'string.max': 'Maximum {#limit} symbol ',
            'string.pattern.base': 'Only letters.',
            'string.required': 'lastName is required'
        }),
    email: Joi.string().trim().required().regex(new RegExp(regExpExpressions.email))
        .messages({
            'string.base': 'Email must be string',
            'string.required': 'Email is required',
            'string.pattern.base': 'Email pattern ivan@ivan.com'
        }),
    phoneNumber: Joi.string().trim().required().regex(new RegExp(regExpExpressions.phoneNumber))
        .messages({
            'string.base': 'Phone number must be string',
            'string.required': 'Phone number is required',
            'string.pattern.base': 'Phone number pattern +380xxxxxxxxx'
        }),
    password: Joi.string().required().regex(new RegExp(regExpExpressions.password))
        .messages({
            'string.base': 'Password must be string',
            'string.required': 'Password is required',
            'string.pattern.base': 'Email pattern ivan@ivan.com'
        }),
});
