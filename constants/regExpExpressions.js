module.exports = {
    name: '^[a-zA-ZА-ЩЬЮЯҐЄІЇа-щьюяґєії]+$',
    email: '^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$',
    password: '^[a-zA-Z0-9]{3,20}$',
    phoneNumber: '^(\\+38)(\\d{10})$'
};
