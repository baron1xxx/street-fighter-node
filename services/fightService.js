const { FightRepository } = require('../repositories/fightRepository');

class FightService {
    create(data) {
        const item = FightRepository.create(data);
        if (!item) {
            return null;
        }
        return item;
    }

    getAll() {
        const items = FightRepository.getAll();
        if (Array.isArray(items) && !items.length) {
            return null;
        }
        return items;
    }

    getOne(id) {
        const item = FightRepository.getOne(id);
        if (!item) {
            return null;
        }
        return item;
    }

    search(search) {
        const item = FightRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    update(id, dataToUpdate) {
        const item = FightRepository.update(id, dataToUpdate);
        if (!item) {
            return null;
        }
        return item;
    }

    delete(id) {
        const items = FightRepository.delete(id);
        if (Array.isArray(items) && !items.length) {
            return null;
        }
        return items;
    }
}

module.exports = new FightService();
