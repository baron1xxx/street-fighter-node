const {FighterRepository} = require('../repositories/fighterRepository');

class FighterService {

    create(data) {
        const item = FighterRepository.create(data);
        if (!item) {
            return null;
        }
        return item;
    }

    getAll() {
        const items = FighterRepository.getAll();
        if (Array.isArray(items) && !items.length) {
            return null;
        }
        return items;
    }

    getOne(id) {
        const item = FighterRepository.getOne(id);
        if (!item) {
            return null;
        }
        return item;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    update(id, dataToUpdate) {
        const item = FighterRepository.update(id, dataToUpdate);
        if (!item) {
            return null;
        }
        return item;
    }

    delete(id) {
        const items = FighterRepository.delete(id);
        if (Array.isArray(items) && !items.length) {
            return null;
        }
        return items;
    }
}

module.exports = new FighterService();
