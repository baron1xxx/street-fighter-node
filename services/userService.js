const {UserRepository} = require('../repositories/userRepository');

class UserService {

    create(data) {
        const item = UserRepository.create(data);
        if (!item) {
            return null;
        }
        return item;
    }

    getAll() {
        const items = UserRepository.getAll();
        if (Array.isArray(items) && !items.length) {
            return null;
        }
        return items;
    }

    getOne(id) {
        const item = UserRepository.getOne(id);
        if (!item) {
            return null;
        }
        return item;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    update(id, dataToUpdate) {
        const item = UserRepository.update(id, dataToUpdate);
        if (!item) {
            return null;
        }
        return item;
    }

    delete(id) {
        const items = UserRepository.delete(id);
        if (Array.isArray(items) && !items.length) {
            return null;
        }
        return items;
    }
}

module.exports = new UserService();
