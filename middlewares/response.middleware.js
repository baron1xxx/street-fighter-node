const responseMiddleware = (req, res, next) => {
    const {err} = req;
    if (!err) {
        res.status(200)
            .json(req.data);
        next();
    } else {
        res.status(err.status)
            .json({
                error: true,
                message: err.message
            });
    }
};

exports.responseMiddleware = responseMiddleware;
