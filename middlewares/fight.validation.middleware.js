const {fighter} = require('../models/fighter');
const { fightCreateValidator, fightUpdateValidator } = require('../validators');


const createFightValid = (req, res, next) => {
    try {
        const {error, value} = fightCreateValidator.validate(req.body);
        if (!error) {
            req.body = value;
            next();
        } else {
            res.status(400)
                .json({
                    error: true,
                    message: error.details[0].message
                })
        }

    } catch (e) {
        req.err = {
            status: 400,
            message: e.message
        };
        next()
    }
};

const updateFightValid = (req, res, next) => {
    try {
        const {error, value} = fightUpdateValidator.validate(req.body);
        if (!error) {
            req.body = value;
            next();
        } else {
            res.status(400)
                .json({
                    error: true,
                    message: error.details[0].message
                })
        }

    } catch (e) {
        req.err = {
            status: 400,
            message: e.message
        };
        next()
    }
};



exports.createFightValid = createFightValid;
exports.updateFightValid = updateFightValid;
