const {fighter} = require('../models/fighter');
const {fighterCreateValidator, fighterUpdateValidator} = require('../validators');

const createFighterValid = (req, res, next) => {
    try {
        const {error, value} = fighterCreateValidator.validate(req.body);
        if (!error) {
            req.body = value;
            next();
        } else {
            res.status(400)
                .json({
                    error: true,
                    message: `${error.details[0].message}`
                })
        }
    } catch (e) {
        req.err = {
            status: 400,
            message: `${e.message} - createFighterValidator`
        };
        next()
    }
}

const updateFighterValid = (req, res, next) => {
    try {
        const {error, value} = fighterUpdateValidator.validate(req.body);
        if (!error) {
            req.body = value;
            next();
        } else {
            res.status(400)
                .json({
                    error: true,
                    message: `${error.details[0].message}`
                })
        }
    } catch (e) {
        req.err = {
            status: 400,
            message: `${e.message} - updateFighterValidator`
        };
        next()
    }
};


exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
