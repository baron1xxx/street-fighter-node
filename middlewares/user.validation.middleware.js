const {user} = require('../models/user');
const {userCreateValidator, userUpdateValidator} = require('../validators');

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    try {
        const {error, value} = userCreateValidator.validate(req.body);
        if (!error) {
            req.body = value;
            next();
        } else {
            res.status(400)
                .json({
                    error: true,
                    message: ` ${error.details[0].path[0]} ${error.details[0].message}`
                })
        }
    } catch (e) {
        req.err = {
            status: 400,
            message: `${e.message} - createUserValidator`
        };
        next()
    }

};

const updateUserValid = (req, res, next) => {
    try {
        const {error, value} = userUpdateValidator.validate(req.body);
        if (!error) {
            req.body = value;
            next();
        } else {
            res.status(400)
                .json({
                    error: true,
                    message: ` ${error.details[0].path[0]} ${error.details[0].message}`
                })
        }
    } catch (e) {
        req.err = {
            status: 400,
            message: `${e.message} - updateUserValidator`
        };
        next()
    }
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
